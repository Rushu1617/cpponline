#include<iostream>
using namespace std;
class Test
{
private:
	 int num1;
	 mutable int num2;
public:
	Test( void ) : num1( 10 ), num2( 20 )
	{
		++ this->num1;	//11
		++ this->num2;	//21
	}
	//Test *const this = &t1;
	void showRecord( void )
	{
		++ this->num1;
		++ this->num2;

		cout<<"Num1	:	"<<this->num1<<endl;
		cout<<"Num2	:	"<<this->num2<<endl;
	}
	//const Test *const this = &t1;
	void printRecord( void )const
	{
		//++ this->num1;	//Not OK
		++ this->num2;	//Not OK

		cout<<"Num1	:	"<<this->num1<<endl;
		cout<<"Num2	:	"<<this->num2<<endl;
	}
};
int main( void )
{
	Test t1;
	t1.printRecord(  );	//t1.printRecord( &t1 );
	return 0;
}

