#include<iostream>
using namespace std;

int main( void )
{
	int *p1 = new int[ 3 ];
	p1[ 0 ] = 10;
	p1[ 1 ] = 20;
	p1[ 2 ] = 30;
	int *p2 = p1;
	for( int index = 0; index < 3; ++ index )
		cout<<p2[ index ]<<endl;

	delete[] p2;
	p2 = nullptr;

	//delete[] p1;	//p1 is dangling pointer
	p1 = nullptr;
	return 0;
}
int main6( void )
{
	int *ptr = new int[ 2 ];
	ptr[ 0 ] = 10;
	ptr[ 1 ] = 20;
	ptr = new int[ 3 ];
	ptr[ 0 ] = 10;
	ptr[ 1 ] = 20;
	ptr[ 2 ] = 30;
	for( int index = 0; index < 3; ++ index )
		cout<<ptr[ index ]<<endl;
	delete ptr;
	ptr = nullptr;
	return 0;
}
int main5( void )
{
	int size = -1;
	int *ptr = new int[ size ];
	if( ptr == NULL )
		cout<<"NULL"<<endl;
	else
		cout<<ptr<<endl;
	//Output : bad_alloc exception
}
int main4( void )
{
	int **ptr = new int*[ 3 ];
	for( int index = 0; index < 3; ++ index )
		ptr[ index ] = new int[ 4 ];

	for( int row = 0; row < 3; ++ row )
	{
		for( int col = 0; col < 4; ++ col )
		{
			cout<<"Enter element	:	";
			cin>>ptr[ row ][ col ];
		}
	}
	for( int row = 0; row < 3; ++ row )
	{
		for( int col = 0; col < 4; ++ col )
		{
			cout<<ptr[ row ][ col ]<<"	";
		}
		cout<<endl;
	}

	for( int index = 0; index < 3; ++ index )
		delete[] ptr[ index ];
	delete[] ptr;
	ptr = nullptr;
	return 0;
}
int main3( void )
{
	int *ptr = new int[ 3 ];
	for( int index = 0; index < 3; ++ index )
	{
		cout<<"Enter element	:	";
		cin>>ptr[ index ];
	}
	for( int index = 0; index < 3; ++ index )
		cout<<ptr[ index ]<<endl;
	delete[] ptr;
	ptr = nullptr;
	return 0;
}
int main2( void )
{
	//int *ptr = new int;
	//int *ptr = new int();
	int *ptr = new int(3);
	cout<<"Value	:	"<<*ptr<<endl;
	delete ptr;	//Memory Deallocation
	ptr = nullptr;
	return 0;
}
int main1( void )
{
	int *ptr = new int;	//Memory Allocation
	//*ptr = 125;
	cout<<"Value	:	"<<ptr<<endl;
	delete ptr;	//Memory Deallocation
	ptr = nullptr;
	return 0;
}
