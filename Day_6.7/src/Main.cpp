#include<iostream>
using namespace std;
void accept_record( int &number )
{
	cout<<"Enter number	:	";
	cin>>number;
}
void print_record( int &result )
{
	cout<<"Result	:	"<<result<<endl;
}
int main( void )
{

	cout<<"Open File"<<endl;

	int num1;
	accept_record( num1 );

	int num2;
	accept_record( num2 );

	int result = num1 / num2;
	print_record( result );

	cout<<"Close File"<<endl;
	return 0;
}
