#include<iostream>
using namespace std;
class Complex
{
public:
	int real;
	int imag;
public:
	/*Complex( void )	//Constructor
	{
		cout<<"Complex( void )"<<endl;
		this->real = 0;
		this->imag = 0;
	}
	Complex( int value )
	{
		cout<<"Complex( int value )"<<endl;
		this->real = value;
		this->imag = value;
	}
	Complex( int real, int imag )	//Constructor
	{
		cout<<"Complex( int real, int imag )"<<endl;
		this->real = real;
		this->imag = imag;
	}*/
	void printRecord( void )
	{
		cout<<"Real Number	:	"<<this->real<<endl;
		cout<<"Imag Number	:	"<<this->imag<<endl;
	}
};
int main( void )
{
	//Complex c1;			//Complex( void )
	//Complex c2( 10 );		//Complex( int value )
	//Complex c3(10,20 );		//Complex( int real, int imag )
	//Complex c4( );	//warning: empty parentheses interpreted as a function declaration
	//Complex c5 = 10;	//Complex c5( 10 );	//Complex( int value )
	//Complex(10,20).printRecord();	//Complex( int real, int imag )
	//Complex c6 = 10, 20;	//Complex c6(10), 20;	//Error
	//Complex c7 = (10, 20);	//Complex c7 = (20);	//Complex c7( 20 );	//Complex( int value )
	Complex c8 = { 10, 20 };
	c8.printRecord();
	return 0;
}
