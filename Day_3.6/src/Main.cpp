#include<iostream>
using namespace std;
namespace naccount
{
	class Account
	{
	private:
		char name[ 30 ];
		int number;
		float balance;
	public:
		void acceptRecord( void );
		void printRecord( void );
	};//end of class
}//end of namespace

using namespace naccount;
void Account::acceptRecord( void )
{
	cout<<"Name	:	";
	cin>> name;
	cout<<"Number	:	";
	cin>>number;
	cout<<"Balance	:	";
	cin>>balance;
}
void Account::printRecord( void )
{
	cout<<"Name	:	"<< name<<endl;
	cout<<"Number	:	"<< number<<endl;
	cout<<"Balance	:	"<< balance<<endl;
}
int main( void )
{
	Account acc;
	acc.acceptRecord( );
	acc.printRecord( );
	return 0;
}
