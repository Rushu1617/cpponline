#include<iostream>
using namespace std;
#include"../include/Math.h"
void accept_record( int &number )
{
	cout<<"Enter number	:	";
	cin>>number;
}

void print_record( int &result )
{
	cout<<"Result	:	"<<result<<endl;
}
int main( void )
{
	int num1;
	accept_record( num1 );

	int num2;
	accept_record( num2 );
	try
	{
		int result = calculate( num1, num2 );
		print_record( result );
	}
	catch( ArithmeticException &ex )
	{
		cout<<ex.getMessage()<<endl;
	}
	catch(...)
	{
		cout<<"Inside generic catch"<<endl;
	}
	return 0;
}
