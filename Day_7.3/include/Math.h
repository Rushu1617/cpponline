
#ifndef MATH_H_
#define MATH_H_
#include"../include/ArithmeticException.h"
using namespace cpponline;

int calculate(int num1, int num2 )throw( ArithmeticException );

#endif /* MATH_H_ */
